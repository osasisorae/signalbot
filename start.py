from __future__ import print_function
from decouple import config

import requests

import gspread
from oauth2client.service_account import ServiceAccountCredentials


token = config('token')
chat_id = config('chat_id')

scopes =["https://spreadsheets.google.com/feeds","https://www.googleapis.com/auth/spreadsheets","https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
creds = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scopes)

client = gspread.authorize(creds)

sheet_name = "Affluent Traders Journal - Daily H & L Tracker by Osas"
sheet = client.open(sheet_name).sheet1

# data = sheet.get_all_records()

# row = sheet.row_values(2)

# col = sheet.col_values(5)

message = sheet.cell(2,5).value


requests.post('https://api.telegram.org/bot{1}/sendMessage?chat_id={2}&text={0}'.format(message,token,chat_id))



